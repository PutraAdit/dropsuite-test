#!/usr/bin/env ruby

require "pathname"

def add(num)
  home = Dir.pwd
	path = Pathname.new(home).children.select { |c| c.directory? }

	if num.empty?
		path.each_with_index do |ph, i|
			puts "List of Folder Paths : \n\n"
			puts "folder #{i+1}" + " : #{ph} " + " => run" + " : ruby test.rb #{i}" + "\n\n"
		end
	else
		path[num.to_i]
	end
end

if ARGV.empty?
	puts 'Please Choose Folder Path' + "\n\n"
	result = add("")
else
	result = add(*ARGV.map(&:to_s))

	a = Dir[File.join(result, '**', '*')].select { |file| File.file?(file) }
	str = []
	content = []
	a.each do |ab|
		ab = File.read(ab)
		result = ab
		content << result
	end

	cc = content.each_with_object(Hash.new(0)) { |o, h| h[o] += 1 }
	max = cc.values.max
	maxs = cc.select { |k, v| v == max }.keys
	res = maxs.join(",").to_s + " " + max.to_s
	puts res
	return res
end

